package com.example.recruiting.model;

public enum VacancyCandidateStatus {
    NEW, DENIED, PASSED
}
