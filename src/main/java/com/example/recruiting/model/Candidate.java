package com.example.recruiting.model;

public record Candidate(Long id, String name, String country, VacancyCandidateStatus status) {
}
