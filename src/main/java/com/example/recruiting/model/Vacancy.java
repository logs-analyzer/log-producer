package com.example.recruiting.model;

public record Vacancy(Long id, String name) {
}
