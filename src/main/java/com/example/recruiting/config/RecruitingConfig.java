package com.example.recruiting.config;

import com.example.recruiting.service.InMemoryJustLoggingVacancyService;
import com.example.recruiting.service.VacancyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RecruitingConfig {

    @Bean
    VacancyService vacancyService() {
        return new InMemoryJustLoggingVacancyService();
    }
}
