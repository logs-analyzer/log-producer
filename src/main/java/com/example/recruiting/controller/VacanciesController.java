package com.example.recruiting.controller;

import com.example.recruiting.model.Candidate;
import com.example.recruiting.model.Vacancy;
import com.example.recruiting.model.VacancyCandidateStatus;
import com.example.recruiting.service.VacancyService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/vacancy")
@RestController
public class VacanciesController {

    private final VacancyService vacancyService;

    public VacanciesController(VacancyService vacancyService) {
        this.vacancyService = vacancyService;
    }

    @PostMapping
    public Vacancy createVacancy(HttpServletRequest request, @RequestBody Vacancy vacancy) {

        return vacancyService.create(vacancy);
    }

    @PostMapping("/{vacancyId}/candidate")
    public Candidate addCandidate(HttpServletRequest request, @PathVariable("vacancyId") Long vacancyId, @RequestBody Candidate candidate) {

        return vacancyService.addCandidate(vacancyId, candidate);
    }

    @PutMapping("/{vacancyId}/candidate/{candidateId}/status/{status}")
    public Candidate changeStatus(
            HttpServletRequest request,
            @PathVariable("vacancyId") Long vacancyId,
            @PathVariable("candidateId") Long candidateId,
            @PathVariable("status") VacancyCandidateStatus status
    ) {

        return vacancyService.changeStatus(vacancyId, candidateId, status);
    }
}
