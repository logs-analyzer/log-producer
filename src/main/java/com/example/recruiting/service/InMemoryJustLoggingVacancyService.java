package com.example.recruiting.service;

import com.example.recruiting.model.Candidate;
import com.example.recruiting.model.Vacancy;
import com.example.recruiting.model.VacancyCandidateStatus;
import lombok.extern.slf4j.Slf4j;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class InMemoryJustLoggingVacancyService implements VacancyService {

    private final Map<Long, Vacancy> vacanciesMap = new HashMap<>();
    private final Map<Long, Map<Long, Candidate>> vacancyCandidatesMap = new HashMap<>();

    @Override
    public Vacancy create(Vacancy vacancy) {

        vacanciesMap.put(vacancy.id(), vacancy);
        vacancyCandidatesMap.put(vacancy.id(), new HashMap());

        log.info(MessageFormat.format("Vacancy with name {0} created", vacancy.name()));

        return vacancy;
    }

    @Override
    public Candidate addCandidate(Long vacancyId, Candidate candidate) {

        var vacancyName = vacanciesMap.get(vacancyId).name();
        vacancyCandidatesMap.get(vacancyId).put(candidate.id(), candidate);

        log.info(
                MessageFormat.format("Candidate from {0} with name “{1}” changed status to “{2}” on vacancy “{3}”.",
                        candidate.country(), candidate.name(), candidate.status(), vacancyName)
        );

        return candidate;
    }

    @Override
    public Candidate changeStatus(Long vacancyId, Long candidateId, VacancyCandidateStatus status) {

        var vacancyName = vacanciesMap.get(vacancyId).name();
        var oldCandidate = vacancyCandidatesMap.get(vacancyId).get(candidateId);

        var newCandidate = new Candidate(candidateId, oldCandidate.name(), oldCandidate.country(), status);

        log.info(
                MessageFormat.format("Candidate from {0} with name “{1}” changed status to “{2}” on vacancy “{3}”.",
                        oldCandidate.country(), oldCandidate.name(), status, vacancyName)
        );

        return newCandidate;
    }
}
