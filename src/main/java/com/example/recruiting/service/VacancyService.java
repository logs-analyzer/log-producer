package com.example.recruiting.service;

import com.example.recruiting.model.Candidate;
import com.example.recruiting.model.Vacancy;
import com.example.recruiting.model.VacancyCandidateStatus;

public interface VacancyService {

    Vacancy create(Vacancy vacancy);

    Candidate addCandidate(Long vacancyId, Candidate candidate);

    Candidate changeStatus(Long vacancyId, Long candidateId, VacancyCandidateStatus status);
}
